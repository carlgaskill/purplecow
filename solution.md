# PurpleCow

Contributors - Carl Gaskill


## Getting started

Terraform build 

Install relevant dependencies 
    
    
    -MACOS
        - I used homebrew, so if homebrew is installed, brew install terraform

    -UBUNTU/DEBIAN
        - curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
        - sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
        - sudo apt install terraform

    -CENTOS/RHEL
        - sudo yum install -y yum-utils
        - sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/$release/hashicorp.repo
        - sudo if needed
        - yum install terraform
    -WINDOWS
        - if chocolatey is installed, use choco install terraform
        - otherwise you can grab the terraform installer here - https://releases.hashicorp.com/terraform/1.1.2/terraform_1.1.2_windows_amd64.zip

Post install

    - cd into project root if needed
    - terraform destroy, terraform apply to test build
    - terraform login (connects to terraform cloud (ideally this would be through a VCS provider)
    - type yes
    - POC API TOKEN = WiQT4PnEjVywVw.atlasv1.PT1zQ4COkaTUmOC8djDDdKnxT5zzB0PW9ixez4fDokwiWLCy3HKWcsHBpTCVyxdRxtc
    - paste the api token into your terminal
    - terraform init
    - terraform apply

- What's Expected to work -

Terraform for IaaC - not the prettiest but boilerplated for time constraints.
Reachability to the api gateway - 

- What's not expected to work -

My function. they are linked to the api gateway, however while im able to execute the ssl_expiry.py script, AND while im able to pull up the api gateway, somethings wrong with the lambda being able to import and parse modules. maybe for future id set a python env yml and add the reqs in there?

To be expected if im honest - never built an API before, nor have I built a lambda from the ground up.

Odd bug encountered where im sure it's something with my code - When going into API gateway detals in the mabda mappings, AWS complains about pulling the url as the name and arn dont match for whatever reason. Good news is if you delete and recreate manually itll work. ---I think i fixed this

- End result - 

 when you go HERE - https://mrn39e4dn5.execute-api.us-east-1.amazonaws.com/test/Purple-Cow-POC-Lambda, you get this

 {
  "errorMessage": "Empty module name",
  "errorType": "ValueError",
  "requestId": "",
  "stackTrace": [
    "  File \"/var/lang/lib/python3.9/importlib/__init__.py\", line 127, in import_module\n    return _bootstrap._gcd_import(name[level:], package, level)\n",
    "  File \"<frozen importlib._bootstrap>\", line 1027, in _gcd_import\n",
    "  File \"<frozen importlib._bootstrap>\", line 961, in _sanity_check\n"
  ]
}

BUT that means we HAVE a lambda thats responding, and we have a working API gateway spun up with terraform. CICD is a little wack because not utilizing version control and actual pipelines, but meh.

