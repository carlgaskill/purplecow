variable "accesskey" {
  description = "accesskey"
  default = ""
}

variable "secretkey" {
  description = "secretkey"
  default = ""
}
variable "region" {
  description = "region"
  default = ""
}
variable "HOSTLIST" {
  description = "List of hosts to do the thing to"
  default = "fearless.tech"
}

variable "EXPIRY_BUFFER" {
  description = "expiry buffer"
  default = "5"
}
