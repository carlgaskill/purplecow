terraform {
  cloud {
    organization = "carlgaskill"

    workspaces {
      name = "fearless-POC"
    }
  }
}

provider "aws" {
  access_key = "${var.accesskey}"
  secret_key = "${var.secretkey}"
  region = "${var.region}"
}

resource "aws_iam_role" "PurpleCow_role" {
  name = "PurpleCow_Role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "PurpleCow_Policy" {
  role       = aws_iam_role.PurpleCow_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "Purple-Cow-POC-Lambda" {

  filename = "./src/Check_for_SSL.py.zip"
  function_name = "Purple-Cow-POC-Lambda"
  description   = "Checks SSL certificate expiry"
  handler       = ".lambda_handler"
  runtime       = "python3.9"
  role          = aws_iam_role.PurpleCow_role.arn
    
environment {
    variables = {
      HOSTLIST = "${var.HOSTLIST}"
      EXPIRY_BUFFER = "${var.EXPIRY_BUFFER}"
    }
  }

  tags = {
    Name = "Purple-Cow-POC-Lambda"
    Owner = "Carl Gaskill"
    Group = "Proof of Concept"
  }
}

resource "aws_cloudwatch_event_rule" "SSLExpirySchedule" {
  name = "SSLExpirySchedule"
  depends_on = [
    aws_lambda_function.Purple-Cow-POC-Lambda
  ]
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "Purple-Cow-POC-Lambda" {
  target_id = "Purple-Cow-POC-Lambda"
  rule = "${aws_cloudwatch_event_rule.SSLExpirySchedule.name}"
  arn = "${aws_lambda_function.Purple-Cow-POC-Lambda.arn}"
}

resource "aws_lambda_permission" "SSLExpirySchedule" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.Purple-Cow-POC-Lambda.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.SSLExpirySchedule.arn}"
}
resource "aws_sns_topic" "FearlesspocSNSTopic" {
  name = "FearlesspocSNSTopic"
}

resource "aws_sns_topic_policy" "FearlesspocSNSTopicPolicy" {
  arn = aws_sns_topic.FearlesspocSNSTopic.arn
  policy = data.aws_iam_policy_document.SNSPolicyDocFearlessPOC.json
}

data "aws_iam_policy_document" "SNSPolicyDocFearlessPOC" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        987164378778,
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.FearlesspocSNSTopic.arn,
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_api_gateway_rest_api" "api" {
 name = "api-gateway"

}

resource "aws_api_gateway_resource" "resource" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  parent_id   = "${aws_api_gateway_rest_api.api.root_resource_id}"
  path_part   = "Purple-Cow-POC-Lambda-test"
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = "${aws_api_gateway_rest_api.api.id}"
  resource_id   = "${aws_api_gateway_resource.resource.id}"
  http_method   = "ANY"
  authorization = "NONE"
  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_resource.resource.id}"
  http_method = "${aws_api_gateway_method.method.http_method}"
  integration_http_method = "ANY"
  type = "HTTP"
  uri = "https://mrn39e4dn5.execute-api.us-east-1.amazonaws.com/test/Purple-Cow-POC-Lambda"
  request_parameters =  {
    "integration.request.path.proxy" = "method.request.path.proxy"
  }
}

resource "aws_api_gateway_integration_response" "Fearless_IR" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id   = "${aws_api_gateway_resource.resource.id}"
  http_method   = "ANY"
  status_code = "200"

  # Transforms the backend JSON response to XML
  response_templates = {
    "application/json" = <<EOF
     
     { set($inputRoot = $input.path('$')),
      $input.path('$.errorMessage')
    }
EOF
  }
}